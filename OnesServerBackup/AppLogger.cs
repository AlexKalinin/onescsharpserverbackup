﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace GoogleChromeUpdate
{
    internal class AppLogger
    {
        private static AppLogger _instance;

        private AppLogger()
        {
        }

        public static AppLogger GetInst()
        {
            return _instance ?? (_instance = new AppLogger());
        }

        public void Log(LogLevel lvl, string msg)
        {

            //todo: write filter here
            //            if (lvl == LogLevel.Debug)
            //                return;

            try
            {
                lock (this)
                {
                    using (var file = new StreamWriter(@"log.txt", true, Encoding.UTF8))
                    {
                        file.WriteLine("[{0}]\t[{1}]\t{2}", DateTime.Now, lvl, msg);
                        file.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error, when writing logs: " + e);
            }
        }

        /// <summary>
        ///     Staff to debug application
        /// </summary>
        /// <param name="msg"></param>
        public void LogDebug(string msg)
        {
            Log(LogLevel.Debug, msg);
        }

        /// <summary>
        ///     Information about imotant things
        /// </summary>
        /// <param name="msg"></param>
        public void LogInfo(string msg)
        {
            Log(LogLevel.Info, msg);
        }

        /// <summary>
        ///     Information about something we want to warn
        /// </summary>
        /// <param name="msg"></param>
        public void LogWarn(string msg)
        {
            Log(LogLevel.Warn, msg);
        }

        /// <summary>
        ///     When error is occured, we writing messages with this marker
        /// </summary>
        /// <param name="msg"></param>
        public void LogError(string msg)
        {
            Log(LogLevel.Error, msg);
        }

        internal enum LogLevel
        {
            /// <summary>
            ///     Staff to debug application
            /// </summary>
            Debug,

            /// <summary>
            ///     Information about imotant things
            /// </summary>
            Info,

            /// <summary>
            ///     Information about something we want to warn
            /// </summary>
            Warn,

            /// <summary>
            ///     When error is occured, we writing messages with this marker
            /// </summary>
            Error
        }
    }
}