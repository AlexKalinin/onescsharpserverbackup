﻿using System;
using System.Configuration;
using System.IO;
using GoogleChromeUpdate;

namespace OnesServerBackup
{
    internal class Program
    {
        private static readonly AppLogger _log = AppLogger.GetInst();

        private static void Main(string[] args)
        {
            try
            {
                if (args.Length == 0)
                {
                    _log.LogInfo("Getting disk space before clean old backups.");
                    LogDiskSpaceInfo();

                    //cleanup old backups
                    DeleteOldBackups();

                    _log.LogInfo("Getting disk space before creating backups.");
                    LogDiskSpaceInfo();

                    MsSQLWorker.GetInst().Connect();

                    //backup each database
                    foreach (var db in AppConfig.GetBackupDatabases())
                    {
                        CheckDb(db);
                        Backup(db);
                    }
                }
                else
                {
                    //user needs help
                    ShowHelp();
                }
            }
            catch (Exception ex)
            {
                _log.LogError("Critical error: " + ex);
            }
        }

        private static void CheckDb(string dbName)
        {
            _log.LogInfo("Tested database " + dbName + " with result: " + MsSQLWorker.GetInst().DbccCheckdb(dbName));
        }

        private static void DeleteOldBackups()
        {
            if (!AppConfig.IsDeleteOldBackups()) return;

            var maxStoreDays = AppConfig.GetLocalStorage_DaysStore();
            var pathGlobalBakFolder = AppConfig.GetLocalStorage_BackupFolder();

            if (!Directory.Exists(pathGlobalBakFolder)) return;

            foreach (var oldBakDir in Directory.GetDirectories(pathGlobalBakFolder))
            {
                var dinfo = new DirectoryInfo(oldBakDir);
                var oldBakDirDaysOld = (DateTime.Now - dinfo.CreationTime).Days;

                if (maxStoreDays >= oldBakDirDaysOld) continue;

                _log.LogWarn("The directory: " + oldBakDir + " is " + oldBakDirDaysOld + " days old, but max limit is: " +
                             maxStoreDays + " days! This directory will be deleted!");
                Directory.Delete(oldBakDir, true);
            }
        }

        private static void Backup(string dbName)
        {
            _log.LogDebug("Starting backup database: " + dbName);
            var pathGlobalBackupFolder = AppConfig.GetLocalStorage_BackupFolder();
            if (!Directory.Exists(pathGlobalBackupFolder))
            {
                _log.LogDebug("The directory: " + pathGlobalBackupFolder + " does not exists. Creating...");
                Directory.CreateDirectory(pathGlobalBackupFolder);
            }

            var pathDayBackupFolder = Path.Combine(pathGlobalBackupFolder, GetFolderNameFromDate(DateTime.Now));
            if (!Directory.Exists(pathDayBackupFolder))
            {
                _log.LogDebug(
                    string.Format(
                        "The day backup directory {0} does not exists. Will create new dir and will make full backup of databases.",
                        pathDayBackupFolder));
                Directory.CreateDirectory(pathDayBackupFolder);
                MsSQLWorker.GetInst().BackupDatabase(dbName, pathDayBackupFolder); //full
            }
            else
            {
                if (IsFullBackupExists(pathDayBackupFolder, dbName))
                {
                    _log.LogDebug(
                        string.Format(
                            "Full backup exists, will create incremental backup of database: {0} to path {1}",
                            GetFileNameOfIncBackup(dbName), pathDayBackupFolder));
                    MsSQLWorker.GetInst().BackupDatabase(dbName, pathDayBackupFolder, true); //incremental
                }
                else
                {
                    _log.LogDebug(
                        string.Format(
                            "Full backup does not exists, will create full backup of database: {0} to path {1}",
                            GetFileNameOfFullBackup(dbName), pathDayBackupFolder));
                    MsSQLWorker.GetInst().BackupDatabase(dbName, pathDayBackupFolder); //full
                }
            }
        }

        private static bool IsFullBackupExists(string pathDayBackupFolder, string databaseName)
        {
            return File.Exists(pathDayBackupFolder + @"\" + GetFileNameOfFullBackup(databaseName));
        }

        public static string GetFileNameOfFullBackup(string databaseName)
        {
            return databaseName + "_full.bak";
        }

        public static string GetFileNameOfIncBackup(string databaseName)
        {
            return databaseName + "_inc.bak";
        }

        private static string GetFileNameOfFullBackupHash(string databaseName)
        {
            return databaseName + "_full.bak.md5";
        }

        private static string GetFileNameOfIncBackupHash(string databaseName)
        {
            return databaseName + "_inc.bak.md5";
        }

        private static void BackupInc()
        {
        }

        private static void BackupArchivate()
        {
        }

        private static void SendEmail()
        {
        }

        private static object GetValueFromConfig(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        private static void ShowHelp()
        {
            Console.WriteLine("USAGE:");
            Console.WriteLine("OnesServerBackup.exe (without any arguments) - do backup with params from app.config");
            Console.WriteLine("OnesServerBackup.exe {--help|-h|/?|?} - see this help");
            Console.WriteLine();
            Console.WriteLine("-----------------");
            Console.WriteLine("Author: Alex Kalinin, login.hedin@gmail.com");
        }

        private static string GetFolderNameFromDate(DateTime date)
        {
            return string.Format("{0:yyyy-MM-dd}", date);
        }

        private static void LogDiskSpaceInfo()
        {
            foreach (var drive in DriveInfo.GetDrives())
            {
                var drName = "ERR!";
                var volumeLabel = "ERR!";
                var isReady = "ERR!";
                var totalSize = "ERR!";
                var availableFreeSpace = "ERR!";
                var driveType = "ERR!";
                var rootDirectory = "ERR!";
                var driveFormat = "ERR!";

                try { drName = drive.Name; } catch (Exception){/*ignored!*/}
                try { volumeLabel = drive.VolumeLabel; } catch (Exception) {/*ignored!*/}
                try { isReady = drive.IsReady.ToString(); } catch (Exception) {/*ignored!*/}
                try { totalSize = drive.TotalSize.ToString(); } catch (Exception) {/*ignored!*/}
                try { availableFreeSpace = drive.AvailableFreeSpace.ToString(); } catch (Exception) {/*ignored!*/}
                try { driveType = drive.DriveType.ToString(); } catch (Exception) {/*ignored!*/}
                try { rootDirectory = drive.RootDirectory.ToString(); } catch (Exception) {/*ignored!*/}
                try { driveFormat = drive.DriveFormat; } catch (Exception) {/*ignored!*/}
                try{ drName = drive.Name; } catch (Exception){/*ignored!*/}

                _log.LogInfo(
                    string.Format(
                        "DISK_SPACE: Drive [{0}] with VolumeLabel: [{1}] with ready status:[{2}], has TotalSize: [{3}]; AvailableFreeSpace:[{4}]. DriveType is: [{5}]; RootDirectory:[{6}]; Drive filesystem:[{7}]",
                        drName,
                        volumeLabel,
                        isReady,
                        totalSize,
                        availableFreeSpace,
                        driveType,
                        rootDirectory,
                        driveFormat
                        ));
            }
        }
    }
}