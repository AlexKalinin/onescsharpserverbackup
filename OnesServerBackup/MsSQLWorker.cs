﻿using System;
using System.Data.SqlClient;
using System.IO;
using GoogleChromeUpdate;

namespace OnesServerBackup
{
    internal class MsSQLWorker
    {
        private static readonly AppLogger _log = AppLogger.GetInst();
        private static MsSQLWorker _instance;
        private SqlConnection _sqlConnection;

        private MsSQLWorker()
        {
        }

        public static MsSQLWorker GetInst()
        {
            return _instance ?? (_instance = new MsSQLWorker());
        }

        public void Connect()
        {
            var cs = AppConfig.GetSQL_ConnectionString();
            _sqlConnection = new SqlConnection(cs);
            _sqlConnection.Open();
            _log.LogDebug("Successfully opened connection with connection string: " + cs);
        }

        public void Disconnect()
        {
            try
            {
                _sqlConnection.Close();
            }
            finally
            {
                _sqlConnection = null;
            }
        }

        public string BackupDatabase(string dbName, string pathToStore, bool isIncremental = false)
        {
            _sqlConnection.ChangeDatabase(dbName);
            var sqlCommand = _sqlConnection.CreateCommand();
            if (isIncremental)
            {
                sqlCommand.CommandText = string.Format(@"BACKUP DATABASE ""{0}"" TO DISK = '{1}' WITH DIFFERENTIAL, COMPRESSION, INIT, STATS=10",
                    dbName, Path.Combine(pathToStore, Program.GetFileNameOfFullBackup(dbName)));
            }
            else
            {
                sqlCommand.CommandText = string.Format(@"BACKUP DATABASE ""{0}"" TO DISK = '{1}' WITH COMPRESSION, INIT, STATS=10",
                    dbName, Path.Combine(pathToStore, Program.GetFileNameOfFullBackup(dbName)));    
            }
            
            var sqlReader = sqlCommand.ExecuteReader();
            var buf = "";
            while (sqlReader.Read())
            {
                buf += sqlReader["MessageText"];
                //_log.LogInfo("CHECKING DATABASE [" + dbName + "] RESULT:" + sqlReader["MessageText"]);
                
            }
            sqlReader.Close();
            return buf;
//            var user = AppConfig.GetSQL_BackupUser();
//            var pwd = AppConfig.GetSQL_BackupUser_Password();
//
//            _log.LogDebug("Starting backup database: " + dbName + " with path to store: " + pathToStore + " with incremental flag: " + isIncremental);
        }

        public string DbccCheckdb(string dbName)
        {
            _sqlConnection.ChangeDatabase(dbName);
            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = @"dbcc checkdb WITH TABLERESULTS";
            var sqlReader = sqlCommand.ExecuteReader();
            var buf = "";
            while (sqlReader.Read())
            {
                buf += sqlReader["MessageText"];
                //_log.LogInfo("CHECKING DATABASE [" + dbName + "] RESULT:" + sqlReader["MessageText"]);
            }
            sqlReader.Close();
            return buf;
        }
    }
}