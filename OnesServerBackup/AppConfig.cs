﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml;

namespace OnesServerBackup
{
    class AppConfig
    {
        private const string CnfFileName = "config.xml";

        public static string GetValueFromConfig(string key)
        {

            var doc = new XmlDocument();

            doc.Load("config.xml");

            var nodes = doc.SelectNodes("AppConfig/" + key);
            if (nodes == null || nodes.Count == 0)
            {
                throw new Exception("The key: " + key + " not found in config file: " + CnfFileName);
            }

            return nodes[0].InnerText;
;
        }

        public static string[] GetBackupDatabases()
        {
            var databases = GetValueFromConfig("SQL_DatabasesToBackup").Trim();
            return databases.Split(';');
        }

//        public static string GetSQL_BackupUser()
//        {
//            return GetValueFromConfig("SQL_BackupUser").Trim();
//        }
//
//        public static string GetSQL_BackupUser_Password()
//        {
//            return GetValueFromConfig("SQL_BackupUser_Password").Trim();
//        }
        public static string GetSQL_ConnectionString()
        {
            return GetValueFromConfig("SQL_ConnectionString").Trim();
        }

        public static string GetLocalStorage_BackupFolder()
        {
            return GetValueFromConfig("LocalStorage_BackupFolder").Trim();
        }

        public static int GetLocalStorage_DaysStore()
        {
            return int.Parse(GetValueFromConfig("LocalStorage_DaysStore").Trim());
        }

        public static bool IsSendEmailReport()
        {
            return bool.Parse(GetValueFromConfig("SMTP_IsSendEmailReport").Trim());
        }

        public static string GetSMTP_SenderEmail()
        {
            return GetValueFromConfig("SMTP_SenderEmail").Trim();
        }

        public static string GetSMTP_SenderEmailPassword()
        {
            return GetValueFromConfig("SMTP_SenderEmailPassword").Trim();
        }

        public static string GetSMTP_server()
        {
            return GetValueFromConfig("SMTP_server").Trim();
        }

        public static string GetSMTP_RecipientEmail()
        {
            return GetValueFromConfig("SMTP_RecipientEmail").Trim();
        }

        public static string GetSMTP_EmailSubject()
        {
            return GetValueFromConfig("SMTP_EmailSubject").Trim();
        }


        public static bool IsDeleteOldBackups()
        {
            return bool.Parse(GetValueFromConfig("LocalStorage_IsDeleteOldBackups").Trim());
        }
    }
}
